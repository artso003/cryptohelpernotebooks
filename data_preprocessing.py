import pandas as pd


def _convert_input_to_series(df, y_value):
    return pd.Series(df[y_value].values, index=df.index)

def _convert_series_to_data_for_model(series):
    data = pd.DataFrame(series.copy())

    data.columns = ["y"]

    return data

def _get_data_with_lags(data, start_lag, end_lag):

    for i in range(start_lag, end_lag):
        data["lag_{}".format(i)] = data.y.shift(i)

    return data

def _get_data_for_model(data):
    y = data.dropna()['y']
    X = data.dropna().drop(['y'], axis=1)
    return X, y

def _add_future_values_hours(data, hours=12):
    for i in range(0, hours):
        data = data.append(pd.DataFrame(index=[data.index[len(data)-1]+pd.offsets.Hour(1)]))

    return data

def _add_future_values_days(data, days=7):
    for i in range(0, days):
        data = data.append(pd.DataFrame(index=[data.index[len(data)-1]+pd.offsets.Day(1)]))

    return data

def _prepare_data_for_prediction(data):
    data = data.tail(sum(pd.isnull(data['y'])))
    data = data.drop('y', axis=1)
    return data

def get_future_data(df, y_value='Close',start_lag=12, end_lag=25 ):
    series = _convert_input_to_series(df, y_value)

    data = _convert_series_to_data_for_model(series)

    data = _add_future_values_hours(data)

    data_with_lags = _get_data_with_lags(data, start_lag, end_lag)

    future_data = _prepare_data_for_prediction(data_with_lags)

    return future_data

def get_data_for_model(df, y_value='Close', start_lag=12, end_lag=25):
    series = _convert_input_to_series(df,y_value)

    data = _convert_series_to_data_for_model(series)

    data_with_lags = _get_data_with_lags(data, start_lag, end_lag)

    X, y = _get_data_for_model(data)

    return X, y


