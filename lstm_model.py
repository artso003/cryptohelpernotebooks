import math
import matplotlib.pyplot as plt
import keras
import pandas as pd
import numpy as np
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers import Dropout
from keras.layers import *
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error

from data_preprocessing import get_data_for_model, get_future_data

import pandas as pd

import metrics


number_of_features = 13
start_lag = 12
end_lag = start_lag + number_of_features
number_of_steps = 3

MODEL_NAME = 'LSTM'

def predict(df):
    

    X, y = get_data_for_model(df, start_lag=start_lag, end_lag=end_lag)
    future_data = get_future_data(df, start_lag=start_lag, end_lag=end_lag)
    future_data_len = len(future_data)
    future_data_ext = X.tail(future_data.shape[0]+(number_of_steps-future_data.shape[0]%number_of_steps))

    model = build_model()

    x_sc, y_sc = build_scalers(X,y,future_data_ext)

    scaled_data = scale_and_shape(X,y,future_data_ext, x_sc, y_sc)

    model.fit(scaled_data[0], scaled_data[1], epochs=20, batch_size=32)

    prediction = y_sc.inverse_transform(model.predict(scaled_data[2])).ravel()[-future_data_len:]

    print(len(prediction))

    high = max(prediction)
    low = min(prediction)
    start = prediction[0]
    end =  prediction[-1]

    future_data['Prediction'] = prediction
    future_data.index = future_data.index.astype(np.int64) // 10**9

    start_date = future_data.index[0]

    return get_model_name(), future_data[['Prediction']], high, low, start, end, metrics.Metrics(build_model(), scaled_data[0], scaled_data[1], y_sc).get_metrics(), start_date, y.values[-1]


def describe_model():
    return 12, 'H'

def get_model_name():
    return MODEL_NAME


def build_scalers(X,y,future_data):
    x_sc = MinMaxScaler(feature_range = (0, 1))
    y_sc = MinMaxScaler(feature_range = (0, 1))
    x_sc.fit(X.append(future_data).values.reshape(-1,1))
    y_sc.fit(y.values.reshape(-1,1))
    return x_sc, y_sc


def scale_and_shape(X, y, future_data, x_sc, y_sc):

    

    X = X[len(X)%(number_of_features*number_of_steps):]
    y = y[len(y)%(number_of_features*number_of_steps):]

    X_scaled = x_sc.transform(X.values.reshape(-1,number_of_features))
    y_scaled = y_sc.transform(y.values.reshape(-1,1))
    future_data_sc = x_sc.transform(future_data.values.reshape(-1,number_of_features))


    X_scaled_shaped = np.reshape(X_scaled, newshape=(-1, number_of_steps, number_of_features))
    y_scaled_shaped = np.reshape(y_scaled, newshape=(-1, number_of_steps, 1))
    future_data_sc_shaped = np.reshape(future_data_sc, newshape=(-1, number_of_steps, number_of_features))

    return X_scaled_shaped, y_scaled_shaped, future_data_sc_shaped


def build_model():

    model = Sequential()
    model.add(LSTM(50, activation='tanh', input_shape=(number_of_steps, number_of_features), return_sequences=True))
    model.add(Dropout(0.2))
    model.add(LSTM(units = 50, return_sequences = True))
    model.add(Dropout(0.2))
    model.add(LSTM(units = 50, return_sequences = True))
    model.add(Dropout(0.2))
    model.add(LSTM(units = 50))
    model.add(Dropout(0.2))
    model.add(Dense(units = number_of_steps))
    model.compile(optimizer = 'adam', loss = 'mse')
    return model

