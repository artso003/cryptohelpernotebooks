from __future__ import print_function

import argparse
import joblib
import os
import pandas as pd
import numpy as np
from data_preprocessing import get_data_for_model, get_future_data
import metrics
from xgboost import XGBRegressor 


MODEL_NAME = 'XGBoost'

def predict(df):
    X, y = get_data_for_model(df)

    future_data = get_future_data(df)

    model = build_model()

    model.fit(X, y)

    prediction = model.predict(future_data)

    high = max(prediction)
    low = min(prediction)
    start = prediction[0]
    end =  prediction[-1]

    future_data['Prediction'] = prediction
    future_data.index = future_data.index.astype(np.int64) // 10**9

    start_date = future_data.index[0]

    return get_model_name(), future_data[['Prediction']], high, low, start, end, metrics.Metrics(build_model(), X, y).get_metrics(), start_date, y.values[-1]

def get_model_name():
    return MODEL_NAME

def describe_model():
    return 12, 'H'

def build_model():
    return XGBRegressor()