import os
import pandas as pd
import numpy as np

from statsmodels.tsa.arima_model import ARIMA
from data_preprocessing import _add_future_values_days

PREDICT_DAYS_NUM = 7

def predict(df):
    series = pd.Series(df['Close'].values, index=df.index)

    data = pd.DataFrame(series.copy())

    data.columns = ["y"]

    data = data.resample('1D').mean()

    data = data.dropna()

    model = ARIMA(data, order=(4,1,0))
    model_fit = model.fit()
    output = model_fit.forecast(steps=PREDICT_DAYS_NUM)
    prediction = output[0]

    future_data = _add_future_values_days(data, PREDICT_DAYS_NUM)
    
    future_data = future_data.tail(sum(pd.isnull(future_data['y'])))
   

    high = max(prediction)
    low = min(prediction)
    start = prediction[0]
    end =  prediction[-1]

    future_data['Prediction'] = prediction
    future_data.index = future_data.index.values.astype(np.int64) // 10 ** 9


    return 'ARIMA', future_data[['Prediction']], high, low, start, end