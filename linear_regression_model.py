import os
import pandas as pd
import numpy as np
from data_preprocessing import get_data_for_model, get_future_data
import metrics

from sklearn.linear_model import LinearRegression

MODEL_NAME = 'LinearRegression'

def predict(df):
    X, y = get_data_for_model(df)

    future_data = get_future_data(df)

    model = build_model()

    model.fit(X, y)

    prediction = model.predict(future_data)

    high = max(prediction)
    low = min(prediction)
    start = prediction[0]
    end =  prediction[-1]

    future_data['Prediction'] = prediction
    future_data.index = future_data.index.astype(np.int64) // 10**9

    start_date = future_data.index[0]

    return get_model_name(), future_data[['Prediction']],  high, low, start, end, metrics.Metrics(build_model(), X, y).get_metrics(), start_date, y.values[-1]

def describe_model():
    return 12, 'H'

def get_model_name():
    return MODEL_NAME

def build_model():
    return LinearRegression()


# if __name__ == '__main__':
#     parser = argparse.ArgumentParser()

#     # Hyperparameters are described here. In this simple example we are just including one hyperparameter.
#     parser.add_argument('--max_leaf_nodes', type=int, default=-1)

#     # Sagemaker specific arguments. Defaults are set in the environment variables.
#     parser.add_argument('--output-data-dir', type=str, default=os.environ['SM_OUTPUT_DATA_DIR'])
#     parser.add_argument('--model-dir', type=str, default=os.environ['SM_MODEL_DIR'])
#     parser.add_argument('--train', type=str, default=os.environ['SM_CHANNEL_TRAIN'])

#     args = parser.parse_args()

#     df = pd.read_json(os.path.join(args.train,'train.json'))

#     

#     model = LinearRegression()

#     model.fit(X, y)

#     joblib.dump(model, os.path.join(args.model_dir, "model.joblib"))



# def model_fn(model_dir):
#     """Deserialized and return fitted model

#     Note that this should have the same name as the serialized model in the main method
#     """
#     model = joblib.load(os.path.join(model_dir, "model.joblib"))
#     return model