import math
import matplotlib.pyplot as plt
import keras
import pandas as pd
import numpy as np
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers import Dropout
from keras.layers import *
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error

from data_preprocessing import get_data_for_model, get_future_data

import pandas as pd

from metrics import get_mae


number_of_features = 13
start_lag = 12
end_lag = start_lag + number_of_features
num_steps = 3

def predict(df):
    

    X, y = get_data_for_model(df, start_lag=start_lag, end_lag=end_lag)
    future_data = get_future_data(df, start_lag=start_lag, end_lag=end_lag)
    future_data_len = len(future_data)
    future_data_ext = X.tail(future_data.shape[0]+(num_steps-future_data.shape[0]%num_steps))
    x_sc = MinMaxScaler(feature_range = (0, 1))
    y_sc = MinMaxScaler(feature_range = (0, 1))
    x_sc.fit(X.append(future_data_ext).values.reshape(-1,1))
    y_sc.fit(y.values.reshape(-1,1))

    X = X[len(X)%(number_of_features*num_steps):]
    y = y[len(y)%(number_of_features*num_steps):]

    X_scaled = x_sc.transform(X.values.reshape(-1,number_of_features))
    y_scaled = y_sc.transform(y.values.reshape(-1,1))
    future_data_sc = x_sc.transform(future_data_ext.values.reshape(-1,number_of_features))


    X_scaled_shaped = np.reshape(X_scaled, newshape=(-1, num_steps, number_of_features))
    y_scaled_shaped = np.reshape(y_scaled, newshape=(-1, num_steps, 1))
    future_data_sc_shaped = np.reshape(future_data_sc, newshape=(-1, num_steps, number_of_features))

    model = build_model()

    model.fit(X_scaled_shaped, y_scaled_shaped, epochs=20, batch_size=32)

    prediction = y_sc.inverse_transform(model.predict(future_data_sc_shaped)).ravel()[-future_data_len:]


    high = max(prediction)
    low = min(prediction)
    start = prediction[0]
    end =  prediction[-1]

    future_data['Prediction'] = prediction
    future_data.index = future_data.index.astype(np.int64) // 10**9

    return 'LSTM', future_data[['Prediction']], high, low, start, end, get_mae(build_model(), X_scaled_shaped, y_scaled_shaped, y_sc)


def build_model():

    model = Sequential()
    model.add(LSTM(50, activation='tanh', input_shape=(num_steps, number_of_features), return_sequences=True))
    model.add(Dropout(0.2))
    model.add(LSTM(units = 50, return_sequences = True))
    model.add(Dropout(0.2))
    model.add(LSTM(units = 50, return_sequences = True))
    model.add(Dropout(0.2))
    model.add(LSTM(units = 50))
    model.add(Dropout(0.2))
    model.add(Dense(units = num_steps))
    model.compile(optimizer = 'adam', loss = 'mse')
    return model
