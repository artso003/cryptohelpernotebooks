from neuralprophet import NeuralProphet
import numpy as np
import pandas as pd

num_prediction_days = 7
MODEL_NAME = 'NeuralProphet'


def predict(df):
    data = prepare_data(df)

    model = build_model()

    metrics = model.fit(data, freq='D')

    future_df = model.make_future_dataframe(data, periods = num_prediction_days)

    prediction = model.predict(future_df)

    prediction_arr = extract_prediction(prediction)

    high = max(prediction_arr)
    low = min(prediction_arr)
    start = prediction_arr[0]
    end =  prediction_arr[-1]

    future_data = pd.DataFrame(data={'Prediction' : prediction_arr})
    future_data.index = prediction.tail(num_prediction_days)['ds'].astype(np.int64) // 10**9

    start_date = future_data.index[0]

    return get_model_name(), future_data[['Prediction']], high, low, start, end, {"MAE" : calculate_mae(data)}, start_date, data['y'].values[-1]

def get_model_name():
    return MODEL_NAME

def describe_model():
    return 7, 'D'


def calculate_mae(data):
    model = build_model()
    df_train, df_val = model.split_df(data, valid_p=0.2, freq='D')
    model.fit(df_train, freq='D')
    return model.test(df_val)['MAE'][0]


def extract_prediction(prediction):
    prediction_arr = []
    i=1
    for _, row in prediction.tail(num_prediction_days).iterrows():
        prediction_arr.append(row[f'yhat{i}'])
        i+=1
    return prediction_arr
    

def prepare_data(df):
    series = pd.Series(df['Close'].values, index=df.index)

    data = pd.DataFrame(series.copy())

    data.columns = ["y"]

    data = data.resample("D").mean()

    data = data.dropna()

    data['ds'] = data.index
    
    return data

def build_model():
    model = NeuralProphet(
        n_lags=30,
        num_hidden_layers=5,
        ar_sparsity=0.01,
        batch_size=32,
        epochs=30,
        n_forecasts=7,
        yearly_seasonality=False,
        weekly_seasonality=False,
        daily_seasonality=False
    )
    
    return model