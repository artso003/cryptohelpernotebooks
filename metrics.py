import numpy as np
from sklearn.metrics import mean_absolute_error, r2_score, mean_squared_error

class Metrics:

            
    def __init__(self, model, X, y, scaler=None):
        self.model = model
        self.X = X
        self.y = y
        self.scaler = scaler

    def get_metrics(self):

        train_ind = int(0.8*len(self.X))
        train_X = self.X[:train_ind]
        train_y = self.y[:train_ind]
        test_X = self.X[train_ind:]
        test_y = self.y[train_ind:]

        self.model.fit(self.X,self.y)
        
        if self.scaler == None:
            y_true = test_y
            y_pred = self.model.predict(test_X)
        else:
            y_true = self.scaler.inverse_transform(self.model.predict(test_X)).ravel()
            y_pred = self.scaler.inverse_transform(test_y.ravel().reshape(-1,1))

        return {
            "MAE" : calculate_mae(y_true, y_pred),
            "MAPE" : calculate_mape(y_true, y_pred),
            "RMSE" : calculate_rmse(y_true, y_pred),
            "R2" : calculate_r2(y_true, y_pred)
        }

def calculate_r2(y_true, y_pred):
    return r2_score(y_true, y_pred)

def calculate_rmse(y_true, y_pred):
    return mean_squared_error(y_true, y_pred, squared=True)

def calculate_mae(y_true, y_pred):
    return mean_absolute_error(y_true, y_pred)

def calculate_mape(y_true, y_pred): 
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100